### STAGE 1: ###
FROM alpine:3.12.1 AS build

#check required arg
ARG APP_NAME
ENV APP_NAME=$APP_NAME

RUN [ -z "$APP_NAME" ] && echo "APP_NAME is required" && exit 1 || true

COPY /dist/$APP_NAME /app

### STAGE 2: Run ###
FROM nginx:1.19.4-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app /usr/share/nginx/html/
